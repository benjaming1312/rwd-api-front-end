import Vue from 'vue'
import Router from 'vue-router'
import company1 from '@/components//layout/company/Company1'

import BootstrapVue from 'bootstrap-vue'

Vue.use(Router)
Vue.use(BootstrapVue)

export default new Router({
  // mode: 'history',
  routes: [
    // 路徑
    {
      path: '/company:id',
      name: 'company',
      component: company1
    },
    {
      path: '/style:id',
      name: 'style',
      component: company1
    },
    {
      path: '/color:id',
      name: 'color',
      component: company1
    },
  ]
})
